#!/usr/bin/env bash

cd rostimote-next && trunk build --release && cp -R dist/* ../rostimote-server/static/

cd ../rostimote-server && cross build --target aarch64-unknown-linux-gnu --release
#cd rostimote-server && cross build --target aarch64-unknown-linux-musl --release

while getopts t:o: flag
do
    case "${flag}" in
        t) target=${OPTARG};;
        o) output=${OPTARG};;
    esac
done

if [ -z ${output} ]; then
    cd ../ && scp ./target/aarch64-unknown-linux-gnu/release/rostimote-server ${target}:${output}
fi
