use std::fmt;

#[cfg(feature = "server")]
use rostilib::Config as RostiConfig;
use serde::{Deserialize, Serialize};
use thiserror::Error;

type PipeNames = Vec<String>;

#[derive(Clone, Debug, Serialize, Deserialize)]
pub enum RostiMsg {
    //TODO find more data to init
    Init {
        config: Config,
        pipes: PipeNames,
        bitrate_values: BitrateValue,
    },
    ConfigUpdate(Config),
    Config(Config),
    //PipeData(Vec<String>),
    Data(RostiData),
    Control(Controls),
    Error(Error),
    Info(String),
    Obs(ObsMsg),
    FlipAudio,
    Mute(bool),
    Audio(bool),
    AudioChannel([u8; 2]),
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct BitrateValue {
    min: u32,
    max: u32,
}

impl BitrateValue {
    pub fn new(min: u32, max: u32) -> Self {
        Self { min, max }
    }
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub enum Controls {
    Start,
    Stop,
    Restart,
    StopViewfinder,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Config {
    pub max_bitrate: u32,
    pub min_bitrate: u32,
    pub server: String,
    pub stream_id: String,
    pub latency: i32,
    pub pipeline: String,
}

impl Config {
    pub fn new(
        max_bitrate: u32,
        min_bitrate: u32,
        server: &str,
        stream_id: &str,
        latency: i32,
        pipeline: &str,
    ) -> Self {
        Self {
            max_bitrate,
            min_bitrate,
            server: server.to_owned(),
            stream_id: stream_id.to_owned(),
            latency,
            pipeline: pipeline.to_owned(),
        }
    }

    pub fn default() -> Self {
        Self {
            max_bitrate: 7000,
            min_bitrate: 500,
            server: String::from(""),
            stream_id: String::from(""),
            latency: 100,
            pipeline: String::from(""),
        }
    }
}

#[cfg(feature = "server")]
impl From<RostiConfig> for Config {
    fn from(value: RostiConfig) -> Self {
        Self::new(
            value.max_bitrate,
            value.min_bitrate,
            &value.ip,
            &value.stream_id,
            value.latency,
            &value.pipe_name,
        )
    }
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub enum ObsMsg {
    Start,
    Stop,
    Status(ObsStatus),
    GetStatus,
    GetLocation,
    Locations(Vec<String>),
    Location(String),
    SetLocation(String),
}

#[derive(Clone, Debug, Serialize, Deserialize, PartialEq, PartialOrd)]
pub enum ObsStatus {
    Offline,
    Starting,
    Started(String),
}

impl fmt::Display for ObsStatus {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let msg = match self {
            Self::Started(_) => "Running",
            Self::Starting => "Starting",
            Self::Offline => "Offline",
        };
        write!(f, "{}", msg)
    }
}

#[derive(Clone, Debug, Serialize, Deserialize, PartialEq, PartialOrd)]
pub struct RostiData {
    pub temp: f32,
    pub curr_bitrate: u32,
    pub status: RostiStatus,
    pub network_stats: Vec<NetworkStat>,
    pub obs_server_status: ObsStatus,
}

impl RostiData {
    pub fn default() -> Self {
        Self {
            temp: 0.0,
            curr_bitrate: 0,
            status: RostiStatus::Offline,
            network_stats: Vec::new(),
            obs_server_status: ObsStatus::Offline,
        }
    }
}

#[derive(Clone, Debug, Serialize, Deserialize, PartialEq, PartialOrd)]
pub enum RostiStatus {
    Offline,
    Connecting,
    Online,
}

impl fmt::Display for RostiStatus {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let msg = match self {
            Self::Connecting => "Connecting",
            Self::Offline => "Disconnected",
            Self::Online => "Connected",
        };
        write!(f, "{}", msg)
    }
}

#[derive(Clone, Debug, Serialize, Deserialize, PartialEq, PartialOrd)]
pub struct NetworkStat {
    pub name: String,
    pub tx_bytes: u64,
    pub tx_bytes_sec: u64,
}

impl NetworkStat {
    pub fn new(name: &str, tx_bytes: u64, tx_bytes_sec: u64) -> Self {
        Self {
            name: name.to_string(),
            tx_bytes,
            tx_bytes_sec,
        }
    }
}

#[derive(Clone, Debug, Serialize, Deserialize, Error)]
pub enum Error {
    #[error("lost connection to the srt server")]
    LostConnection,
    #[error("an unknown error occurred")]
    Unknown,
    #[error("{0}")]
    Custom(String),
    #[error("lost connection to video src (cable/capture device)")]
    LostVideoSrc,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Logs {}
