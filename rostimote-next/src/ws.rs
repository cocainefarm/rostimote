use async_std::sync::RwLock;
use futures::{
    stream::{SplitSink, SplitStream},
    SinkExt, StreamExt,
};
use reqwasm::websocket::{futures::WebSocket, Message};
use serde::Serialize;
use std::sync::Arc;
use wasm_bindgen_futures::spawn_local;

#[derive(Clone)]
pub struct DioxusWs {
    pub url: String,
    pub sender: Arc<RwLock<SplitSink<WebSocket, Message>>>,
    pub receiver: Arc<RwLock<SplitStream<WebSocket>>>,
    pub is_open: Arc<RwLock<bool>>,
}

impl DioxusWs {
    pub fn new(url: &str) -> DioxusWs {
        let ws = WebSocket::open(url).unwrap();

        let (sender, receiver) = ws.split();
        let sender = Arc::new(RwLock::new(sender));
        let receiver = Arc::new(RwLock::new(receiver));

        DioxusWs {
            url: url.to_string(),
            sender,
            receiver,
            is_open: Arc::new(RwLock::new(false)),
        }
    }

    /// Sends a reqwasm Message
    pub fn send(&self, msg: Message) {
        let sender = self.sender.clone();
        let is_open = self.is_open.clone();

        spawn_local(async move {
            let is_open = *is_open.read().await;

            if is_open {
                let mut sender = sender.write().await;
                sender.send(msg).await.ok();
            }
        });
    }

    pub fn set_open(&self, open: bool) {
        let is_open = self.is_open.clone();
        let sender = self.sender.clone();

        spawn_local(async move {
            let mut is_open = is_open.write().await;
            *is_open = open;

            let mut sender = sender.write().await;
            sender.close().await.ok();
        });
    }

    /// Sends a plaintext string
    pub fn _send_text(&self, text: String) {
        let msg = Message::Text(text);
        self.send(msg);
    }

    /// Sends data that implements Serialize as JSON
    pub fn _send_json<T: Serialize>(&self, value: &T) {
        let json = serde_json::to_string(value).unwrap();
        let msg = Message::Text(json);
        self.send(msg);
    }

    pub async fn reconnect(&self) {
        let ws = WebSocket::open(&self.url).unwrap();

        let (sender, receiver) = ws.split();

        {
            let mut self_sender = self.sender.write().await;
            *self_sender = sender;
        }

        {
            let mut self_receiver = self.receiver.write().await;
            *self_receiver = receiver;
        }
    }
}
