use common::{ObsMsg, ObsStatus, RostiMsg};
use dioxus::prelude::*;

use crate::{send_ws_msg, Obs};

pub fn render_obs_controls<'a>(cx: Scope<'a>, obs: &'a UseRef<Obs>) -> Element<'a> {
    let obs_data = obs.read();
    let region = obs_data.selected.clone();
    let region = region.as_str();

    let (obs_color, obs_msg, obs_ws) = match obs_data.status {
        ObsStatus::Offline => ("info", "start", ObsMsg::Start),
        _ => ("error", "stop", ObsMsg::Stop),
    };

    cx.render(rsx! {
        div {
            class: "center",
            style: "margin-bottom: 0.75rem",
            div { class: "header", "Obs server | {obs_data.status}" }
            div {
                "Server location: ",
                select {
                    onchange: move |e| {
                        obs.with_mut(|o| {
                            o.selected = e.value.clone();
                            let msg = RostiMsg::Obs(ObsMsg::SetLocation(e.value.clone()));
                            send_ws_msg(&cx, msg);
                        });
                    },
                    if obs_data.selected.is_empty() {
                        cx.render(rsx!{option {
                            disabled: true, selected: true, value: "", "EMPTY"
                        }})
                    }
                    obs_data.regions.iter().map(|name| {
                        cx.render(rsx!{
                            option {
                                selected: region == name,
                                value: "{name}",
                                "{name}"
                            }
                        })
                    })
                }
            }
            button {
                class: "button sm-button bg-{obs_color}",
                onclick: move |_| {
                    send_ws_msg(&cx, RostiMsg::Obs(obs_ws.clone()));
                },
                "{obs_msg} OBS server"
            }
        }
    })
}
