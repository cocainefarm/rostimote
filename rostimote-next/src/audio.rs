use dioxus::prelude::*;
use std::time::Duration;
use common::{ObsMsg, ObsStatus, RostiMsg};
use crate::send_ws_msg;

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum AudioState {
    Update,
    Waiting
}

impl std::fmt::Display for AudioState {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            AudioState::Update => "".fmt(f),
            AudioState::Waiting => "fade-out".fmt(f)
        }
    }
}

#[derive(Debug, Clone, Copy)]
pub struct AudioChannel {
    pub running: bool,
    pub mute: bool,
    pub left: u8,
    pub right: u8,
    pub max_left: MaxAudio,
    pub max_right: MaxAudio,
}

impl AudioChannel {
    pub fn update(&mut self, left: u8, right: u8) {
        self.left = left;
        self.right = right;

        self.max_left.update(left);
        self.max_right.update(right);
    }

    pub fn update_max(&mut self) {
        self.max_left.update_time(self.left);
        self.max_right.update_time(self.right);
    }

    pub fn timeout_left(&self) -> bool {
        self.max_left.timeout()
    }

    pub fn timeout_right(&self) -> bool {
        self.max_right.timeout()
    }

    pub fn waiting_left(&mut self) {
        self.max_left.waiting();
    }

    pub fn waiting_right(&mut self) {
        self.max_right.waiting();
    }

    pub fn clear(&mut self) {
        *self = AudioChannel::default();
    }

    pub fn clear_left(&mut self) {
        self.max_left = MaxAudio::default();
    }

    pub fn clear_right(&mut self) {
        self.max_right = MaxAudio::default();
    }
}

impl Default for AudioChannel {
    fn default() -> Self {
        Self { 
            running: false,
            mute: false,
            left: 60, 
            right: 60, 
            max_left: MaxAudio::default(), 
            max_right: MaxAudio::default(), 
        } 
    }
}

#[derive(Debug, Clone, Copy)]
pub struct MaxAudio {
    tsp: f64,
    pub value: u8,
    pub state: AudioState,
}

impl MaxAudio {
    pub fn new(value: u8) -> Self {
        let tsp = instant::now();
        Self {
            tsp,
            value,
            state: AudioState::Update
        }
    }

    pub fn update(&mut self, update_value: u8) {
        let tsp = instant::now();
        if self.value > update_value {
            self.value = update_value;
            self.tsp = tsp;
            self.state = AudioState::Update;
        }
    }

    pub fn update_time(&mut self, value: u8) {
        let tsp = instant::now();
        self.value = value;
        self.tsp = tsp;
        self.state = AudioState::Update;
    }

    pub fn timeout(&self) -> bool {
        let tsp = instant::now();
        self.state == AudioState::Update && tsp - self.tsp > 7000.0
    }

    pub fn waiting(&mut self) {
        self.state = AudioState::Waiting;
    }
}

impl Default for MaxAudio {
    fn default() -> Self {
        Self { tsp: 0.0, value: 60, state: AudioState::Waiting }
    } 
}

pub fn render_audio<'a>(cx: Scope<'a>, audio: &'a UseState<AudioChannel>) -> Element<'a> {
    if audio.timeout_left() {
        audio.make_mut().waiting_left();
        let audio_remove = audio.clone();
        cx.push_future(async move {
            log::debug!("start timer");
            async_std::task::sleep(Duration::from_secs(2)).await;
            log::debug!("waited 7 sec");
            audio_remove.make_mut().clear_left();
        });
    }

    if audio.timeout_right() {
        audio.make_mut().waiting_right();
        let audio_remove = audio.clone();
        cx.push_future(async move {
            //log::debug!("start timer");
            async_std::task::sleep(Duration::from_secs(2)).await;
            //log::debug!("waited 7 sec");
            audio_remove.make_mut().clear_right();
        });
    }

    let (audio_color, audio_msg) = if audio.running {
        ("error", "hide")
    } else {
        ("info", "show")
    };

    let (mute_color, mute_msg) = if audio.mute {
        ("error", "unmute")
    } else {
        ("info", "mute")
    };

    cx.render(rsx!{
        div {
            style: "margin: 0.75rem",
            div { class: "header", "Audio" }
            div {
                class: "flex",
                button {
                    class: "button sm-button bg-{audio_color}",
                    onclick: move |_| {
                        send_ws_msg(&cx, RostiMsg::Audio(!audio.running));
                        if audio.running {
                            audio.make_mut().clear();
                            audio.make_mut().clear();
                            audio.make_mut().clear();
                        } else {
                            audio.make_mut().running = true;
                        }
                    },
                    "{audio_msg} audio"
                }
                button {
                    class: "button sm-button bg-{mute_color}",
                    onclick: move |_| {
                        send_ws_msg(&cx, RostiMsg::Mute(!audio.mute));
                        audio.make_mut().mute = !audio.mute;
                    },
                    "{mute_msg} audio"
                }
                button {
                    class: "button sm-button bg-info",
                    onclick: move |_| {
                        send_ws_msg(&cx, RostiMsg::FlipAudio);
                    },
                    "flip audio"
                }
            }
            div {
                class: "flex",
                style: "margin-top: 0.5rem",
                div { style: "width: 1.5rem", "L" },
                div {
                    style: "width: 100%",
                    div { 
                        class: "bg-info bar",
                        style: "width: {(1.0 - audio.left as f32 / 60.0) * 100.0}%;",
                    }
                    div {
                        class: "flex",
                        style: "width: {(1.0 - audio.max_left.value as f32 / 60.0) * 100.0}%; position: inherit; margin-top: -1.2rem;",
                        div { style: "width: 100%" }
                        div {
                            style: "height: 1.2rem; width: 1.2rem; border-radius: 1.75rem",
                            class: "bg-warning {audio.max_left.state}",
                        }
                    }
                }
            }
            div {
                class: "flex",
                style: "margin-top: 0.5rem",
                div { style: "width: 1.5rem", "R" },
                div {
                    style: "width: 100%",
                    div { 
                        class: "bg-info bar",
                        style: "width: {(1.0 - audio.right as f32 / 60.0) * 100.0}%;",
                    }
                    div {
                        class: "flex",
                        style: "width: {(1.0 - audio.max_right.value as f32 / 60.0) * 100.0}%; position: inherit; margin-top: -1.2rem;",
                        div { style: "width: 100%" }
                        div {
                            style: "height: 1.2rem; width: 1.2rem; border-radius: 1.75rem",
                            class: "bg-warning {audio.max_right.state}",
                        }
                    }
                }
            }
        }
    })
}