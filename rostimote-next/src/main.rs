use dioxus::prelude::*;
//use dioxus_websocket_hooks::{use_ws_context, use_ws_context_provider_json};
use common::{Config, NetworkStat, ObsMsg, ObsStatus, RostiData, RostiMsg, RostiStatus};
use futures::StreamExt;
use reqwasm::websocket::Message;
use std::time::Duration;

mod alert;
mod audio;
mod controls;
mod misc;
#[cfg(feature = "obs-server")]
mod obs_controls;
mod ws;

use alert::*;
use audio::*;
use misc::*;
use ws::*;

//TODO get max/min from server
const MAX_BITRATE: i32 = 14000;
const MIN_BITRATE: i32 = 300;

fn main() {
    wasm_logger::init(wasm_logger::Config::default());
    // launch the web app
    dioxus_web::launch(app);
}

#[derive(Debug, Clone)]
pub struct Obs {
    status: ObsStatus,
    regions: Vec<String>,
    selected: String,
}

impl Obs {
    fn new() -> Self {
        Self {
            status: ObsStatus::Offline,
            regions: Vec::new(),
            selected: "".to_string(),
        }
    }
}

fn app(cx: Scope) -> Element {
    let alerts = use_ref(&cx, Vec::new);
    //let alerts = use_ref(&cx, || vec![Alert::new("testing", Color::Warning)]);
    let alerts2 = alerts.clone();

    let obs = use_ref(&cx, Obs::new);
    let obs_ws = obs.clone();

    let audio = use_state(cx, || AudioChannel::default());
    let audio_ws = audio.clone();

    let max_network = use_state(cx, || 100);
    let _max_network_ws = max_network.clone();

    let config = use_state(&cx, || Config::default());
    let pipe_names = use_state(&cx, || Vec::new());

    let mut data = RostiData::default();
    data.network_stats
        .push(NetworkStat::new("testing", 100_000_000, 25));
    data.network_stats
        .push(NetworkStat::new("testing", 200_000_000, 70));
    let data = use_state(&cx, || data);
    let config_ws = config.clone();
    let pipe_names_ws = pipe_names.clone();
    let data2 = data.clone();

    cx.use_hook(move || {
        let window = web_sys::window().unwrap();
        let document = window.document().unwrap();
        let location = document.location().unwrap();
        let hostname = location.hostname().unwrap();
        let ws = cx.provide_context(DioxusWs::new(&format!("ws://{}:6969", hostname)));
        let receiver = ws.receiver.clone();

        let alerts3 = alerts2.clone();
        let handler = move |msg| match msg {
            Message::Text(text) => {
                let json = serde_json::from_str::<RostiMsg>(&text);

                match json {
                    Ok(json) => {
                        log::debug!("ws msg: {:?}", json);
                        match json {
                            RostiMsg::Init { config, pipes, bitrate_values } => {
                                *config_ws.make_mut() = config;
                                *pipe_names_ws.make_mut() = pipes;
                            }
                            RostiMsg::Data(rdata) => {
                                *data2.make_mut() = rdata;
                            }
                            RostiMsg::Config(new_config) => {
                                *config_ws.make_mut() = new_config;
                            }
                            RostiMsg::Error(error) => {
                                let alert = Alert::new(&error.to_string(), Color::Error);
                                alerts3.with_mut(|a| a.push(alert));
                            }
                            RostiMsg::Info(info) => {
                                let alert = Alert::new(&info, Color::Success);
                                alerts3.with_mut(|a| a.push(alert));
                            }
                            RostiMsg::Obs(obs_msg) => {
                                match obs_msg {
                                    ObsMsg::Status(status) => obs_ws.with_mut(|o| o.status = status),
                                    ObsMsg::Locations(location) => {
                                        obs_ws.with_mut(|o| o.regions = location)
                                    }
                                    ObsMsg::Location(location) => {
                                        obs_ws.with_mut(|o| o.selected = location)
                                    }
                                    _ => {}
                                }
                            }
                            RostiMsg::AudioChannel(left_right) => {
                                let mut channel = audio_ws.make_mut();
                                channel.update(left_right[0], left_right[1]);
                            }
                            _ => {}
                        }
                    }
                    Err(e) => log::error!("error: {:?}", e),
                }
            }
            Message::Bytes(_) => {}
        };


        cx.push_future(async move {
            loop {
                let mut err = None;

                {
                    let mut receiver = receiver.write().await;
                    while let Some(msg) = receiver.next().await {
                        match msg {
                            Ok(msg) => {
                                ws.set_open(true);
                                handler(msg)
                            },
                            Err(e) => {
                                err = Some(e);
                            }
                        }
                    }
                }

                if let Some(err) = err {
                    ws.set_open(false);

                    let alert = Alert::new("Lost connection to server, reconnecting....", Color::Error);
                    alerts2.with_mut(|a| a.push(alert));

                    log::error!(
                        "Error while trying to receive message over websocket, reconnecting in 1s...\n{:?}", err
                    );

                    async_std::task::sleep(Duration::from_millis(1000)).await;

                    ws.reconnect().await;
                }
            }
        })
    });

    let d = data.get();
    let c = config.get();
    let _a = alerts.read();

    let status_color = match d.status {
        RostiStatus::Connecting => "warning",
        RostiStatus::Offline => "error",
        RostiStatus::Online => "success",
    };

    cx.render(rsx! {
        style { include_str!("./style.css") }
        main {
            render_alert(&cx, &alerts),
            div {
                class: "pill",
                div {
                    class: "flex w-full",
                    style: "margin: 0rem 0.25rem",
                    div {
                        class: "pill-item",
                        style: "color: rgb({color(d.temp as i32, 70, 20, true)})",
                        format!("{:.2} °C", d.temp)
                    },
                    div {
                        class: "pill-item",
                        style: "color: rgb({color(d.curr_bitrate as i32, MAX_BITRATE, 0, false)})",
                        format!("{} Kb/s", d.curr_bitrate)
                    },
                    div { class: "pill-item {status_color}", "{d.status}" },
                }
            }
            div {
                class: "container",
                controls::render_controls(&cx, &d.status, config),
                div {
                    d.network_stats
                     .iter()
                     .map(|network| {
                         if max_network.get() < &network.tx_bytes_sec {
                             *max_network.make_mut() = network.tx_bytes_sec;
                         }
                         let bar = (network.tx_bytes_sec as f64 / *max_network.get() as f64) * 100.0;
                         rsx!{
                            div {
                                class: "flex",
                                style: "padding: 0.25rem 0.75rem",
                                div { style: "width: 8rem; margin-right: 0.25rem; overflow: hidden;", "{network.name}" }
                                div {
                                    style: "width: 100%; text-align: center",
                                    div {
                                        style: "position: absolute; left: 5rem; right: 5rem; margin: auto;",
                                        "{network.tx_bytes_sec} KB/s",
                                    }
                                    div {
                                        style: "width: {bar}%;",
                                        class: "bg-info bar",
                                    }
                                }
                                div { style: "width: 8rem; text-align: right; margin-left: 0.25rem", "{format_tx_data(network.tx_bytes)}" }
                            }
                        }
                     })
                }
                div {
                    class: "bitrate-container",
                    div { class: "header", "Srt Config" }
                    div {
                        class: "flex",
                        div { style: "width: 6rem", "server ip:" }
                        input {
                            class: "blur text-input",
                            value: "{config.server}",
                            placeholder: "server address",
                            oninput: move |e| {
                                config.make_mut().server = e.inner().value.clone();
                            }
                        }
                    }
                    div {
                        style: "margin-top: 0.25rem",
                        class: "flex",
                        div { style: "width: 6rem;", "srt id:" },
                        input {
                            class: "blur text-input",
                            value: "{config.stream_id}",
                            placeholder: "srt stream id",
                            oninput: move |e| {
                                config.make_mut().stream_id = e.inner().value.clone();
                            }
                        }
                    }
                    div {
                        class: "flex",
                        style: "margin-top: 0.25rem",
                        "Selected pipeline: ",
                        select {
                            onchange: move |e| {
                                config.make_mut().pipeline = e.value.clone();
                            },
                            if config.pipeline.is_empty() {
                                cx.render(rsx!{option {
                                    disabled: true, selected: true, value: "", "EMPTY"
                                }})
                            }
                            pipe_names.iter().map(|name| {
                                cx.render(rsx!{
                                    option {
                                        selected: &config.pipeline == name,
                                        value: "{name}",
                                        "{name}"
                                    }
                                })
                            })
                        }
                    }
                    div {
                        style: "text-align: center; width: 100%;",
                        button {
                            class: "button sm-button bg-info",
                            onclick: move |_| {
                                let msg = RostiMsg::Config(c.clone());
                                send_ws_msg(&cx, msg);
                            },
                            "save"
                        }
                    }
                },
                render_audio(&cx, audio),
                {
                    #[cfg(feature = "obs-server")]
                    obs_controls::render_obs_controls(&cx, obs)
                }
            }
        }
    })
}
