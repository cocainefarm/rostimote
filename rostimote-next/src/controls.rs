use common::{Config, RostiMsg, RostiStatus};
use dioxus::prelude::*;

use crate::{send_ws_msg, MAX_BITRATE, MIN_BITRATE};

pub fn render_controls<'a>(
    cx: Scope<'a>,
    status: &RostiStatus,
    config: &'a UseState<Config>,
) -> Element<'a> {
    let c = config.get();
    let reset = use_state(cx, || false);
    let viewfinder = use_state(cx, || false);

    cx.render(rsx! {
        div {
            class: "flex",
            style: "padding: 0px 0.25rem",
            match status {
                RostiStatus::Online | RostiStatus::Connecting => render_slider(&cx, &reset),
                RostiStatus::Offline => {
                    if !*reset.get() {
                        *reset.make_mut() = true;
                    }
                    cx.render(rsx!{
                        button {
                            class: "button bg-success",
                            onclick: move |_| {
                                let msg = RostiMsg::Control(common::Controls::Start);
                                send_ws_msg(&cx, msg);
                            },
                            "start rosti"
                        }
                    })
                }
            }
            button {
                class: "button bg-info",
                onclick: move |_| {
                    if *viewfinder.get() {
                        *viewfinder.make_mut() = false;
                        let msg = RostiMsg::Control(common::Controls::StopViewfinder);
                        send_ws_msg(&cx, msg);
                    } else {
                        *viewfinder.make_mut() = true;
                    }
                },
                if *viewfinder.get() {
                    "close viewfinder"
                } else {
                    "open viewfinder"
                }
            }
        }
        if *viewfinder.get() {
            let tsp = instant::now();
            cx.render(rsx!{
                div {
                    class: "viewfinder",
                    img { src: "/video.mjpg?t={tsp}" },
                }
            })
        }
        div {
            class: "bitrate-container",
            //style: "margin: 0.75rem 0.75rem",
            //class: "card",
            div {
                class: "flex",
                div { style: "width: 100%; text-align: left; margin-left: 0.55rem", "min" }
                div { class: "header", "Bitrate" }
                div { style: "width: 100%; text-align: right; margin-right: 0.5rem", "max" }
            }
            div {
                class: "flex",
                input {
                    r#type: "number",
                    class: "text-input",
                    style: "width: 2.5rem; text-align: center; margin-right: 0.25rem",
                    value: "{c.min_bitrate}",
                    onchange: |e| {
                        let value = e.inner().value.parse().unwrap_or(0);
                        if value <= c.max_bitrate {
                            config.make_mut().min_bitrate = value;
                        } else {
                            config.make_mut().min_bitrate = c.max_bitrate;
                        }
                    },
                    oninput: move |e| {
                        match e.inner().value.parse::<u32>() {
                            Ok(_value) => {
                            },
                            Err(_) => cx.needs_update()
                        }
                    }
                }
                div {
                    class: "slider-container",
                    div { class: "slider-track" }
                    input {
                        r#type: "range",
                        min: "{MIN_BITRATE}",
                        max: "{MAX_BITRATE + 800}",
                        value: "{c.min_bitrate}",
                        step: "100",
                        onchange: move |e| {
                            let value = e.data.value.parse().unwrap_or(0);
                            let mut new_config = config.get().to_owned();
                            new_config.min_bitrate = value;
                            //config.make_mut().min_bitrate = value;
                            let msg = RostiMsg::Config(new_config);
                            send_ws_msg(&cx, msg);
                        },
                        oninput: move |e| {
                            let value = e.inner().value.parse().unwrap_or(0);
                            if value <= c.max_bitrate {
                                config.make_mut().min_bitrate = value;
                            } else {
                                config.make_mut().min_bitrate = c.max_bitrate;
                            }
                        },
                    },
                    input {
                        r#type: "range",
                        min: "{MIN_BITRATE - 800}",
                        max: "{MAX_BITRATE}",
                        value: "{c.max_bitrate}",
                        step: "100",
                        onchange: move |e| {
                            let value = e.data.value.parse().unwrap_or(0);
                            let mut new_config = config.get().to_owned();
                            new_config.max_bitrate = value;
                            config.make_mut().max_bitrate = value;
                            let msg = RostiMsg::Config(new_config);
                            send_ws_msg(&cx, msg);
                        },
                        oninput: move |e| {
                            let value = e.inner().value.parse().unwrap_or(0);
                            if value >= c.min_bitrate {
                                config.make_mut().max_bitrate = value;
                            } else {
                                config.make_mut().max_bitrate = c.min_bitrate;
                            }
                        },
                        //onmouseup: move |e| {
                        //}
                    }
                }
                input {
                    r#type: "number",
                    class: "text-input",
                    style: "width: 2.5rem; text-align: center; margin-left: 0.25rem",
                    value: "{c.max_bitrate}",
                    onchange: |e| {
                        let value = e.inner().value.parse().unwrap_or(0);
                        if value >= c.min_bitrate {
                            config.make_mut().max_bitrate = value;
                        } else {
                            config.make_mut().max_bitrate = c.min_bitrate;
                        }
                    },
                    oninput: move |e| {
                        match e.inner().value.parse::<u32>() {
                            Ok(_value) => {
                            },
                            Err(_) => cx.needs_update()
                        }
                    }
                }
                //"{c.max_bitrate}",
            }
        }
    })
}

fn render_slider<'a>(cx: Scope<'a>, reset: &UseState<bool>) -> Element<'a> {
    let _moving = use_state(cx, || false);
    let _position = use_state(cx, || 0);
    let moving_left_pos = use_state(cx, || 0);
    let width = use_state(cx, || 0);

    let _left = moving_left_pos.get();
    let window = web_sys::window().unwrap();
    let document = window.document().unwrap();
    let slider_box = document.get_element_by_id("slider-box");
    if let Some(s_box) = slider_box {
        let w = s_box.client_width() - 40;
        if &w != width.get() {
            *width.make_mut() = w;
        }
    }

    let pos = use_state(cx, || 0.0);

    //if *reset.get() && !*moving.get() && moving_left_pos.get() == width.get() {
    //    *moving_left_pos.make_mut() = 0;
    //    *reset.make_mut() = false;
    //}

    if *reset.get() && pos.get() == &100.0 {
        *pos.make_mut() = 0.0;
        *reset.make_mut() = false;
    }

    cx.render(rsx! {
        div {
            id: "slider-box",
            class: "slider-box",
            input {
                class: "stop-slider",
                r#type: "range",
                value: "{pos}",
                min: 0.0,
                max: 100.0,
                oninput: move |e| {
                    let value = e.inner().value.parse().unwrap_or(0.0);
                    *pos.make_mut() = value;
                },
                onchange: move |e| {
                    let value = e.inner().value.parse().unwrap_or(0);
                    if value != 100 {
                        *pos.make_mut() = 0.0;
                    } else {
                        let msg = RostiMsg::Control(common::Controls::Stop);
                        send_ws_msg(&cx, msg.clone());
                    }
                }
            }
            div {
                style: "left: calc({pos}% + ({3.0 - pos * 0.4}px));",
                class: "slider-icon",
                ">>"
            }
            //button {
            //    class: "button bg-error",
            //    left: "{moving_left_pos.get()}px",
            //    onpointerdown: move |e| {
            //        *position.make_mut() = e.inner().screen_x;
            //        *moving.make_mut() = true;
            //    },
            //    onpointerup: move |e| {
            //        *position.make_mut() = 0;
            //        *moving.make_mut() = false;
            //        if left < width.get() {
            //            *moving_left_pos.make_mut() = 0;
            //        }
            //    },
            //    onpointermove: move |e| {
            //        if *moving.get() {
            //            if moving_left_pos.get() == width.get() {
            //                *moving.make_mut() = false;
            //                let msg = RostiMsg::Control(common::Controls::Stop);
            //                send_ws_msg(&cx, msg.clone());
            //                send_ws_msg(&cx, msg.clone());
            //                send_ws_msg(&cx, msg);
            //            } else {
            //                let pos = e.inner().screen_x;
            //                let old_pos = position.get();
            //                *moving_left_pos.make_mut() = (moving_left_pos + (pos - old_pos) as i32).clamp(0, *width.get());
            //                *position.make_mut() = pos;
            //            }
            //        }
            //    },
            //    ">>"
            //},
            div {
                class: "slider-box-text",
                "stop rosti"
            }
        }
    })
}
