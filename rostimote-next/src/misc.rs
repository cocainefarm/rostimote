use common::RostiMsg;
use dioxus::prelude::*;
use number_prefix::*;

use crate::ws::DioxusWs;
use crate::Message;

pub fn send_ws_msg(cx: &Scope, msg: RostiMsg) {
    let ws = cx.consume_context::<DioxusWs>().unwrap();
    log::debug!("sending data ws: {:?}", msg);
    let json = serde_json::to_string(&msg).unwrap();
    ws.send(Message::Text(json))
}

#[derive(Debug, Clone, Copy)]
pub enum Color {
    Success,
    Warning,
    Error,
}

impl std::fmt::Display for Color {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let color = match self {
            Self::Error => "error",
            Self::Warning => "warning",
            Self::Success => "success",
        };
        write!(f, "{}", color)
    }
}

#[derive(Clone, Debug)]
pub struct RGB {
    pub red: i32,
    pub green: i32,
    pub blue: i32,
}

impl RGB {
    pub fn new(red: i32, green: i32, blue: i32) -> Self {
        Self { red, green, blue }
    }
}

impl std::fmt::Display for RGB {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}, {}, {}", self.red, self.green, self.blue)
    }
}

pub fn color(input: i32, max_value: i32, min_value: i32, invert: bool) -> RGB {
    let yellow = RGB::new(255, 240, 11);
    let (first, last) = if invert {
        let red = RGB::new(202, 71, 84);
        let green = RGB::new(33, 149, 1);
        (green, red)
    } else {
        let red = RGB::new(202, 71, 84);
        let green = RGB::new(33, 149, 1);
        (red, green)
    };

    if input <= min_value {
        first
    } else {
        let p = (input - min_value) as f32 / max_value as f32;
        if p > 0.5 {
            let p = (p - 0.5) * 2.0;
            let r = ((last.red - yellow.red) as f32 * p) as i32 + yellow.red;
            let g = ((last.green - yellow.green) as f32 * p) as i32 + yellow.green;
            let b = ((last.blue - yellow.blue) as f32 * p) as i32 + yellow.blue;
            RGB::new(r, g, b)
        } else if p == 0.5 {
            yellow
        } else {
            //let p = (p - 0.5) * 2.0;
            let r = ((yellow.red - first.red) as f32 * p) as i32 + first.red;
            let g = ((yellow.green - first.green) as f32 * p) as i32 + first.green;
            let b = ((yellow.blue - first.blue) as f32 * p) as i32 + first.blue;
            RGB::new(r, g, b)
        }
    }
}

pub fn format_tx_data(tx: u64) -> String {
    let result = match NumberPrefix::decimal(tx as f64) {
        NumberPrefix::Standalone(bytes) => {
            format!("{} B", bytes)
        }
        NumberPrefix::Prefixed(prefix, n) => {
            format!("{:.2} {}B", n, prefix)
        }
    };
    result
}
