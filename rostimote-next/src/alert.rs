use std::time::Duration;
use dioxus::prelude::*;

use crate::misc::Color;

#[derive(Clone)]
pub struct Alert {
    tsp: f64,
    msg: String,
    color: Color,
}

impl Alert {
    pub fn new(msg: &str, color: Color) -> Self {
        let time = instant::now();
        Self {
            tsp: time,
            msg: msg.to_string(),
            color,
        }
    }

    pub fn get_tsp(&self) -> f64 {
        self.tsp
    }

    pub fn cmp_tsp(&self, tsp: f64) -> bool {
        self.tsp == tsp
    }
}

pub fn render_alert<'a>(cx: Scope<'a>, alerts: &'a UseRef<Vec<Alert>>) -> Element<'a> {
    let alerts4 = alerts.clone();
    let alert = alerts.read();
    cx.render(match alert.first() {
        Some(alert) => {
            let tsp = alert.get_tsp();
            cx.push_future(async move {
                async_std::task::sleep(Duration::from_secs(10)).await;
                remove_alert(tsp, &alerts4);
            });

            rsx!{
                div {
                    class: "alert-box",
                    div {
                        onclick: move |_| {remove_alert(tsp, alerts)},
                        class: "alert bg-{alert.color}",
                        "{alert.msg}"
                    }
                }
            }
        }
        None => rsx!{ "" }
    })
}

pub fn remove_alert(tsp: f64, alerts: &UseRef<Vec<Alert>>) {
    alerts.with_mut(|a| {
        if let Some(alert) = a.first() {
            if alert.cmp_tsp(tsp) {
                a.remove(0);
            }
        }
    });
}
