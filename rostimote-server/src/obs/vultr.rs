use std::collections::HashMap;

use anyhow::Result;
use http::{HeaderMap, HeaderValue, StatusCode};
use serde::{Deserialize, Serialize, de::DeserializeOwned};
use common::{ObsStatus, Error};
use reqwest::blocking::Response;

use crate::obs::Obs;

const API_URL: &str = "https://api.vultr.com/v2/";
const SERVER_ID: &str = "vcg-a16-2c-8g-2vram";
const SNAPSHOT_NAME: &str = "obs";

#[derive(Debug)]
pub struct Vultr {
    client: reqwest::blocking::Client,
    server_id: String,
    status: ObsStatus,
    server_ip: String,
    selected_region: String,
    regions: Vec<(String, String)>,
    snapshot_id: String,
}

impl Obs for Vultr {
    fn start_up(&mut self) -> Result<Option<ObsStatus>> {
        self.request_plans()?;
        self.request_regions()?;
        self.request_snapshot()?;
        self.request_running_servers()
        //Ok(())
    }

    fn start_server(&mut self) -> Result<Option<ObsStatus>, Error> {
        if !self.selected_region.is_empty() {
            match self.status {
                ObsStatus::Offline => {
                    log::info!("starting obs server");
                    let data = VultrCreateInstance {
                        region: self.selected_region.to_owned(),
                        plan: SERVER_ID.to_string(),
                        snapshot_id: self.snapshot_id.to_owned(),
                        backups: "disabled".to_string(),
                        label: "obs".to_string()
                    };
                    let resp = self.post_request("instances", data)
                        .map_err(|e| {
                            log::error!("error while creating obs server: {:?}", e);
                            Error::Custom("error while creating obs server".to_string())
                        })?;
                    let resp: VultrTypes = resp
                        .json()
                        .map_err(|e| {
                            log::error!("error while creating obs server: {:?}", e);
                            Error::Custom("error while creating obs serveR".to_string())
                        })?;
                    if let VultrTypes::Instance(instance) = resp {
                        self.status = ObsStatus::Starting;
                        self.server_id = instance.id;
                        self.server_ip = instance.main_ip;
                        //self.update_server(instance);
                    }
                    Ok(Some(self.status.to_owned()))
                }
                _ => {
                    log::error!("Server allready started");
                    Err(Error::Custom("Server allready started".to_string()))
                }
            }
        } else {
            log::error!("server location not selected");
            Err(Error::Custom("Server location not selected".to_string()))
        }
    }

    fn stop_server(&mut self) -> Result<Option<ObsStatus>, Error> {
        match self.status {
            ObsStatus::Offline => {
                log::error!("server allready offline");
                Err(Error::Custom("Server allready offline".to_string()))
            }
            ObsStatus::Starting => {
                log::error!("server currently starting, please wait until started");
                Err(Error::Custom("server currently starting, please retry after server ist started".to_string()))
            }
            _ => {
                log::info!("deleting obs server");
                self.delete_request(&format!("instances/{}", self.server_id))
                    .map_err(|e| {
                        log::error!("error while deleting server: {:?}", e);
                        Error::Custom("Error while deleting server".to_string())
                    })?;
                self.clear_server();
                Ok(Some(self.status.to_owned()))
            }
        }
    }

    fn update(&mut self) -> Result<Option<ObsStatus>, Error> {
        if !self.server_id.is_empty() {
            let resp = self.get_request(&format!("instances/{}", self.server_id), None)
                .map_err(|e| {
                    log::error!("error while requesting server: {:?}", e);
                    Error::Custom("Error while requesting obs server, wrong api key?".to_string())
                })?;
            println!("{:?}", resp.status());
            match resp.status() {
                StatusCode::OK => {
                    let resp: VultrTypes = resp.json()
                        .map_err(|e| {
                            log::error!("Error while parsing data: {:?}", e);
                            Error::Custom("".to_string())
                        })?;
                    if let VultrTypes::Instance(instance) = resp {
                        println!("{:?}", instance);
                        return Ok(self.update_server(instance));
                    }
                }
                StatusCode::BAD_REQUEST => {
                    if self.clear_server() {
                        return Ok(Some(ObsStatus::Offline));
                    }
                }
                _ => {}
            }
        } else {
            log::error!("Cant check server, server dose not exist");
        }
        Ok(None)
    }

    fn set_location(&mut self, location: &str) -> Result<(), Error> {
        match self.regions.iter().find(|(_, name)| name == location) {
            Some((id, _)) => {
                self.selected_region = id.to_string();
                Ok(())
            },
            None => {
                let msg = "Location not found";
                log::error!("{}", msg);
                Err(Error::Custom(msg.to_string()))
            }
        }
    }

    fn get_locations(&self) -> Vec<String> {
        self.regions.iter().map(|(_, name)| name.clone()).collect()
    }

    fn get_location(&self) -> Option<String> {
        let (_, name) = self.regions.iter().find(|(id, _)| id == &self.selected_region)?;
        Some(name.to_owned())
    }

    fn get_status(&self) -> ObsStatus {
        self.status.clone()
    }
}

impl Vultr {
    pub fn new(api_token: &str) -> Result<Self> {
        let client = reqwest::blocking::ClientBuilder::new();
        let mut header = HeaderMap::new();
        header.append("accept", HeaderValue::from_str("*/*")?);
        header.append("Authorization", HeaderValue::from_str(&format!("Bearer {}", api_token))?);
        let client = client.default_headers(header).build()?;
        let vultr = Self {
            client,
            server_id: "".to_string(),
            status: ObsStatus::Offline,
            server_ip: "".to_string(),
            selected_region: "".to_string(),
            regions: Vec::new(),
            snapshot_id: "".to_string()
        };
        Ok(vultr)
    }

    fn request_plans(&mut self) -> Result<()> {
        let para = vec![("type", "vcg"), ("per_page", "500")];
        let resp = self.get_request("plans", Some(para))?;
        let resp: VultrResp = resp.json()?;
        if let VultrTypes::Plans(plans) = resp.vultr_type {
            let plan = plans.iter().find(|p| p.id == SERVER_ID);
            match plan {
                Some(p) => {
                    for region in &p.locations {
                        self.regions.push((region.to_string(), "".to_string()));
                    }
                }
                None => log::error!("Faild to find vultr plan")
            }
        }
        Ok(())
    }

    fn request_regions(&mut self) -> Result<()> {
        let para = vec![("per_page", "500")];
        let resp = self.get_request("regions", Some(para))?;
        let resp: VultrResp = resp.json()?;
        if let VultrTypes::Regions(regions) = resp.vultr_type {
            for region in regions {
                let r = self.regions.iter_mut().find(|(id, _)| id == &region.id);
                if let Some((_, name)) = r {
                    *name = region.city;
                }
            }
        }
        Ok(())
    }

    fn request_running_servers(&mut self) -> Result<Option<ObsStatus>> {
        log::info!("requesting running obs server");
        let resp = self.get_request("instances", None)?;
        let resp: VultrResp = resp.json()?;
        if let VultrTypes::Instances(servers) = resp.vultr_type {
            for server in servers {
                if server.label.contains("obs") {
                    println!("{:?}", server);
                    println!("{:?}", server.status());
                    log::info!("found running obs server");
                    return Ok(self.update_server(server));
                }
            }
        }
        Ok(None)
    }

    fn request_snapshot(&mut self) -> Result<()> {
        log::info!("requesting snapshot id");
        let resp = self.get_request("snapshots", None)?;
        let resp: VultrResp = resp.json()?;
        if let VultrTypes::Snapshots(snapshots) = resp.vultr_type {
            let snapshot = snapshots.iter().find(|s| s.description == SNAPSHOT_NAME);
            match snapshot {
                Some(s) => self.snapshot_id = s.id.to_string(),
                None => log::error!("Faild to find the obs snapshot")
            }
        }
        Ok(())
    }

    fn get_request(&self, path: &str, paras: Option<Vec<(&str, &str)>>) -> Result<Response> {
        let url = format!("{}{}", API_URL, path);
        let url = match paras {
            Some(p) => reqwest::Url::parse_with_params(&url, p)?,
            None => reqwest::Url::parse(&url)?
        };
        let resp = self.client.get(url)
            .send()?;
        Ok(resp)
    }

    fn post_request<T: Serialize>(&self, path: &str, data: T) -> Result<Response> {
        let resp = self.client.post(&format!("{}{}", API_URL, path))
            .json(&data)
            .send()?;
        Ok(resp)
    }

    fn delete_request(&self, path: &str) -> Result<Response> {
        let resp = self.client.delete(&format!("{}{}", API_URL, path))
            .send()?;
        Ok(resp)
    }

    fn clear_server(&mut self) -> bool {
        if self.status != ObsStatus::Offline {
            self.status = ObsStatus::Offline;
            self.server_id.clear();
            self.server_ip.clear();
            true
        } else {
            false
        }
    }

    fn update_server(&mut self, server: VultrInstance) -> Option<ObsStatus> {
        let status = server.status()?;
        if self.status != status {
            self.status = status.clone();
            match status {
                ObsStatus::Starting | ObsStatus::Started(_) => {
                    self.server_id = server.id;
                    self.server_ip = server.main_ip;
                }
                ObsStatus::Offline => {
                    self.server_id.clear();
                    self.server_ip.clear();
                }
            }
            Some(status)
        } else {
            None
        }
    }
}

#[derive(Serialize, Deserialize, Debug, Clone)]
struct VultrResp {
    meta: VultrMeta,

    #[serde(flatten)]
    vultr_type: VultrTypes
}

#[derive(Serialize, Deserialize, Debug, Clone)]
struct VultrMeta {
    total: i32,
    links: VultrLinks
}

#[derive(Serialize, Deserialize, Debug, Clone)]
struct VultrLinks {
    next: String,
    prev: String
}

#[derive(Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "lowercase")]
enum VultrTypes {
    Regions(Vec<VultrRegion>),
    Instances(Vec<VultrInstance>),
    Instance(VultrInstance),
    Plans(Vec<VultrPlan>),
    Snapshots(Vec<VultrSnapshot>)
}

#[derive(Serialize, Deserialize, Debug, Clone)]
struct VultrInstance {
    pub id: String,
    pub os: String,
    pub ram: i32,
    pub disk: i32,
    pub main_ip: String,
    pub vcpu_count: i32,
    pub region: String,
    pub plan: String,
    pub date_created: String,
    pub status: String,
    pub allowed_bandwidth: i32,
    pub netmask_v4: String,
    pub gateway_v4: String,
    pub power_status: String,
    pub server_status: String,
    pub v6_network: String,
    pub v6_main_ip: String,
    pub v6_network_size: i32,
    pub label: String,
    pub internal_ip: String,
    pub kvm: String,
    pub hostname: String,
    pub os_id: i32,
    pub app_id: i32,
    pub image_id: String,
    pub firewall_group_id: String,
    pub features: Vec<String>,
    pub tags: Vec<String>
}

impl VultrInstance {
    fn status(&self) -> Option<ObsStatus> {
        println!("{}", self.server_status);
        match self.server_status.as_str() {
            "none" => Some(ObsStatus::Starting),
            "locked" => Some(ObsStatus::Starting),
            "installingbooting" => Some(ObsStatus::Starting),
            "ok" => Some(ObsStatus::Started(self.main_ip.to_owned())),
            _ => None
        }
    }
}

#[derive(Serialize, Deserialize, Debug, Clone)]
struct VultrRegion {
    id: String,
    city: String,
    country: String,
    continent: String,
    options: Vec<String>
}

#[derive(Serialize, Deserialize, Debug, Clone)]
struct VultrPlan {
    id: String,
    vcpu_count: i32,
    ram: i32,
    disk: i32,
    disk_count: i32,
    bandwidth: i32,
    monthly_cost: f32,
    #[serde(rename = "type")]
    plan_type: String,
    locations: Vec<String>
}

#[derive(Serialize, Deserialize, Debug, Clone)]
struct VultrSnapshot {
    id: String,
    date_created: String,
    description: String,
    size: i64,
    compressed_size: i64,
    status: String,
    os_id: i32,
    app_id: i32
}

#[derive(Serialize, Deserialize, Debug, Clone)]
struct VultrCreateInstance {
    region: String,
    plan: String,
    snapshot_id: String,
    backups: String,
    label: String
}
