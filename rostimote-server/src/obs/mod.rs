use anyhow::Result;
use common::{RostiMsg, ObsMsg, ObsStatus, Error, Config};
use rostilib::{Config as RostiConfig, Rostcoder};
use tokio::sync::broadcast::Sender;
use std::{env, thread, time::{Duration, Instant}, sync::{Arc, RwLock}};

pub mod vultr;

trait Obs {
    //NOTE
    // creates new server and all pre setup
    //fn new(api_token: &str) -> Result<Self>;
    fn start_up(&mut self) -> Result<Option<ObsStatus>>;
    //NOTE
    // starts a new obs server/instace at vps provider
    fn start_server(&mut self) -> Result<Option<ObsStatus>, Error>;
    //NOTE
    // stops a running obs server/instace at vps provider
    fn stop_server(&mut self) -> Result<Option<ObsStatus>, Error>;
    //NOTE
    // gets current update to server if running etc
    fn update(&mut self) -> Result<Option<ObsStatus>, Error>;

    fn get_locations(&self) -> Vec<String>;

    fn get_location(&self) -> Option<String>;

    fn set_location(&mut self, location: &str) -> Result<(), Error>;

    fn get_status(&self) -> ObsStatus;

    //fn status_to_obs_status(&self) -> Result<()>;
    //async fn start_up() -> Result<()>;
    //async fn get_running_server() -> Result<()>;
}

fn get_provider() -> Result<impl Obs> {
    for (name, value) in env::vars() {
        match name.as_str() {
            "VULTR_API_KEY" => {
                return Ok(vultr::Vultr::new(&value)?);
            }
            _ => {}
        }
    }
    Err(anyhow::Error::msg("No api key found for any provider"))
}

pub fn run(tx: Sender<RostiMsg>, config: Arc<RwLock<RostiConfig>>, mut rosti: Rostcoder) -> Result<()> {
    let mut obs = get_provider()?;

    let mut retry = 0;
    loop {
        if retry > 3 {
            //TODO error something
            break;
        }
        match obs.start_up() {
            Ok(status) => {
                if let Some(ObsStatus::Started(ip)) = status {
                    let mut c = config.write().unwrap();
                    let (_, port) = c.ip.split_once(':').unwrap_or(("", ""));
                    c.ip = format!("{}:{}", ip, port);
                    let new_config = c.clone();
                    drop(c);
                    rosti.update_config(&new_config)?;
                    //NOTE dunno what i did here maybe its essentail maybe not
                    //let config = Config::new(
                    //    new_config.max_bitrate,
                    //    new_config.min_bitrate,
                    //    &new_config.ip,
                    //    &new_config.stream_id,
                    //    new_config.latency,
                    //
                    //);
                    tx.send(RostiMsg::Config(new_config.into()))?;
                }
                break
            }
            Err(e) => {
                //TODO check for internet
                log::error!("faild to startup obs provider, api key wrong or no internet | {:?}", e);
                let msg = common::Error::Custom("Obs provider initialize error, wrong key? no internet?".to_string());
                tx.send(RostiMsg::Error(msg));
            }
        }
        thread::sleep(Duration::from_secs(60));
        retry += 1;
    }

    let mut rx = tx.subscribe();
    let mut time = Instant::now();
    loop {
        if let Ok(RostiMsg::Obs(msg)) = rx.try_recv() {
            handle_obs_msg(&mut obs, msg, &tx)?;
        }
        if time.elapsed().as_secs() >= 60 {
            if let Some(status) = obs.update()? {
                if let ObsStatus::Started(ip) = &status {
                    let mut c = config.write().unwrap();
                    let (_, port) = c.ip.split_once(':').unwrap_or(("", ""));
                    c.ip = format!("{}:{}", ip, port);
                    let new_config = c.clone();
                    drop(c);
                    rosti.update_config(&new_config)?;
                    //NOTE dunno what i did here maybe its essentail maybe not again
                    //let config = Config::new(
                    //    new_config.max_bitrate,
                    //    new_config.min_bitrate,
                    //    &new_config.ip,
                    //    &new_config.stream_id,
                    //    new_config.latency
                    //);
                    tx.send(RostiMsg::Config(new_config.into()))?;
                }
                tx.send(RostiMsg::Obs(ObsMsg::Status(status)))?;
            }
            time = Instant::now();
        }
        thread::sleep(Duration::from_millis(500));
    }
}

fn handle_obs_msg(obs: &mut impl Obs, msg: ObsMsg, tx: &Sender<RostiMsg>) -> Result<()> {
    log::debug!("obs got msg: {:?}", msg);
    match msg {
        ObsMsg::Start => {
            match obs.start_server() {
                Ok(Some(status)) => {
                    tx.send(RostiMsg::Obs(ObsMsg::Status(status)))?;
                }
                Err(e) => {
                    tx.send(RostiMsg::Error(e))?;
                }
                _ => {}
            }
        }
        ObsMsg::Stop => {
            match obs.stop_server() {
                Ok(Some(status)) => {
                    tx.send(RostiMsg::Obs(ObsMsg::Status(status)))?;
                }
                Err(e) => {
                    tx.send(RostiMsg::Error(e))?;
                }
                _ => {}
            }
        }
        ObsMsg::SetLocation(location) => obs.set_location(&location)?,
        ObsMsg::GetLocation => {
            let locations = obs.get_locations();
            tx.send(RostiMsg::Obs(ObsMsg::Locations(locations)))?;
            if let Some(l) = obs.get_location() {
                tx.send(RostiMsg::Obs(ObsMsg::Location(l)))?;
            }
        }
        ObsMsg::GetStatus => {
            let status = obs.get_status();
            tx.send(RostiMsg::Obs(ObsMsg::Status(status)))?;
        }
        _ => {}
    }
    Ok(())
}
