use http::header::CONTENT_TYPE;
//use std::sync::mpsc::Receiver;
use anyhow::Result;
use rostilib::Rostcoder;

use std::convert::Infallible;
use std::net::SocketAddr;

use http::StatusCode;
use hyper::service::{make_service_fn, service_fn};
use hyper::{Body, Method, Request, Response, Server};

use include_dir::{include_dir, Dir};

//use tokio::sync::broadcast::{Sender, Receiver};
use tokio_stream::{wrappers::BroadcastStream, StreamExt};

use tokio::sync::broadcast::Receiver;

pub async fn run(rosti: rostilib::Rostcoder) -> Result<()> {
    //TODO add to config
    let web_addr = SocketAddr::from(([0, 0, 0, 0], 8000));

    let web_service = make_service_fn(move |_| {
        let rosti2 = rosti.clone();
        async { Ok::<_, Infallible>(service_fn(move |req| web(req, rosti2.clone()))) }
    });

    let server = Server::bind(&web_addr)
        .http1_preserve_header_case(true)
        .http1_title_case_headers(true)
        .serve(web_service);

    log::debug!("Listening on http://{}", web_addr);

    if let Err(e) = server.await {
        log::debug!("server error: {}", e);
    }

    Ok(())
}

//const INDEX: &str = include_str!("../static/index.html");
//const JS: &str = include_str!(env!("ROSTIMOTE_JS_PATH"));
//const WASM: &[u8] = include_bytes!(env!("ROSTIMOTE_WASM_PATH"));

const NEXT_DIR: Dir<'_> = include_dir!("$CARGO_MANIFEST_DIR/../rostimote-next/dist");

async fn web(req: Request<Body>, rosti: Rostcoder) -> Result<Response<Body>> {
    match (req.method(), req.uri().path()) {
        (&Method::GET, "/stop") => {
            rosti.stop_viewfidner()?;
            Ok(Response::default())
        }
        (&Method::GET, "/video.mjpg") => {
            let rx = rosti.start_viewfidner()?;
            serve_req(rx).await
        }
        (&Method::GET, "/") | (&Method::GET, "/index.html") => {
            let file = NEXT_DIR
                .get_file("index.html")
                .unwrap()
                .contents_utf8()
                .unwrap();
            let body = Body::from(file);
            let resp = Response::builder().body(body).unwrap();
            Ok(resp)
        }
        _ => {
            let mut path = req.uri().path().to_string();
            if path.starts_with('/') && path.len() > 1 {
                path.remove(0);
                if let Some(file) = NEXT_DIR.get_file(&path) {
                    if path.contains(".wasm") {
                        let file = file.contents();
                        let body = Body::from(file);
                        let resp = Response::builder()
                            .header(CONTENT_TYPE, "application/wasm")
                            .body(body)
                            .unwrap();
                        return Ok(resp);
                    } else {
                        let file = file.contents_utf8().unwrap();
                        let body = Body::from(file);
                        let resp = Response::builder()
                            .header(CONTENT_TYPE, "text/javascript")
                            .body(body)
                            .unwrap();
                        return Ok(resp);
                    }
                }
            }
            let body = Body::empty();
            Ok(Response::builder()
                .status(StatusCode::NOT_FOUND)
                .body(body)
                .unwrap())
        }
    }
}

async fn serve_req(rx: Receiver<Vec<u8>>) -> Result<Response<Body>> {
    let rx = BroadcastStream::new(rx);
    let result_stream = rx.map(|buffer| hyper::Result::Ok(buffer.unwrap_or(Vec::new())));
    let body = Body::wrap_stream(result_stream);

    Ok(Response::builder()
        .status(StatusCode::OK)
        .header(
            "Content-Type",
            "multipart/x-mixed-replace; boundary=--7b3cc56e5f51db803f790dad720ed50a",
        ) // MJPEG stream.
        .body(body)
        .unwrap())
}
