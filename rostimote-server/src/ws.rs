use futures_util::StreamExt;

use futures::SinkExt;
use tokio::net::{TcpListener, TcpStream};
use tokio::sync::broadcast::Sender;
use tungstenite::protocol::Message;

use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::{Arc, RwLock};

use common::{BitrateValue, Controls, ObsMsg, RostiData, RostiMsg};
use rostilib::{Config as RostiConfig, Rostcoder};

pub async fn start_ws_server(
    rosti: Rostcoder,
    tx: Sender<RostiMsg>,
    data: Arc<RwLock<RostiData>>,
    rosti_config: Arc<RwLock<RostiConfig>>,
    restart: Arc<AtomicBool>,
) {
    let addr = "0.0.0.0:6969";

    // Create the event loop and TCP listener we'll accept connections on.
    let try_socket = TcpListener::bind(&addr).await;
    let listener = try_socket.expect("Failed to bind");
    log::debug!("Listening on: {}", addr);

    // Let's spawn the handling of each connection in a separate task.
    while let Ok((stream, addr)) = listener.accept().await {
        log::debug!("Incoming TCP connection from: {}", addr);
        //TODO its ugly
        let _ = tx.send(RostiMsg::Obs(ObsMsg::GetStatus));
        let _ = tx.send(RostiMsg::Obs(ObsMsg::GetLocation));
        tokio::spawn(handle_connection(
            stream,
            rosti.clone(),
            tx.clone(),
            data.clone(),
            rosti_config.clone(),
            restart.clone(),
        ));
    }
}

async fn handle_connection(
    raw_stream: TcpStream,
    mut rosti: Rostcoder,
    tx: Sender<RostiMsg>,
    //mut rx: Receiver<RostiMsg>,
    data: Arc<RwLock<RostiData>>,
    rosti_config: Arc<RwLock<RostiConfig>>,
    restart: Arc<AtomicBool>,
) {
    let ws_stream = tokio_tungstenite::accept_async(raw_stream)
        .await
        .expect("Error during the websocket handshake occurred");

    let (mut outgoing, mut incoming) = ws_stream.split();

    let data = data.read().unwrap().clone();
    let data = RostiMsg::Data(data);
    let data_json = serde_json::to_string(&data).unwrap();
    let _ = outgoing.send(Message::Text(data_json)).await;

    let rconfig = rosti_config.read().unwrap().clone();

    let pipes = rconfig
        .pipes
        .get_inner_values()
        .into_iter()
        .map(|(names, _)| names.to_owned())
        .collect();
    let config = rconfig.into();
    //drop(rconfig);

    let bitrate_values = BitrateValue::new(500, 7000);

    let init_msg = RostiMsg::Init {
        config,
        pipes,
        bitrate_values,
    };
    let init_json = serde_json::to_string(&init_msg).unwrap();
    //let config = RostiMsg::Config(config);
    //let config_json = serde_json::to_string(&config).unwrap();
    let _ = outgoing.send(Message::Text(init_json)).await;

    let mut rx = tx.subscribe();
    tokio::spawn(async move {
        while let Ok(msg) = rx.recv().await {
            let json = serde_json::to_string(&msg).unwrap();
            let _ = outgoing.send(Message::Text(json)).await;
        }
    });

    //TODO test after reloading to stop rosti
    while let Some(Ok(Message::Text(msg))) = incoming.next().await {
        log::debug!("recv msg: {:?}", msg);
        let data = serde_json::from_str(&msg).unwrap();
        match data {
            RostiMsg::Control(Controls::Start) => {
                if !restart.load(Ordering::Relaxed) {
                    restart.store(true, Ordering::Relaxed);
                    //*restart.get_mut() = true;
                }
                rosti.start().unwrap();
            }
            RostiMsg::Control(Controls::Stop) => {
                if restart.load(Ordering::Relaxed) {
                    restart.store(false, Ordering::Relaxed);
                    //*restart.get_mut() = false;
                }
                rosti.stop().unwrap();
            }
            RostiMsg::Control(Controls::StopViewfinder) => {
                rosti.stop_viewfidner().unwrap();
            }
            RostiMsg::Config(config) => {
                let mut c = rosti_config.write().unwrap();
                c.min_bitrate = config.min_bitrate;
                c.max_bitrate = config.max_bitrate;
                c.ip = config.server;
                c.pipe_name = config.pipeline;
                rosti.update_config(&c).unwrap();
                drop(c);
                //TODO add min bitrate update
                rosti.update_max_bitrate(config.max_bitrate).unwrap();
            }
            //TODO
            RostiMsg::Obs(msg) => {
                let _ = tx.send(RostiMsg::Obs(msg));
            }
            RostiMsg::Audio(running) => {
                rosti.update_audio(running).unwrap();
            }
            RostiMsg::Mute(mute) => {
                rosti.update_mute(mute).unwrap();
            }
            RostiMsg::FlipAudio => {
                rosti.flip_audio().unwrap();
            }
            _ => {}
        }
    }
}
