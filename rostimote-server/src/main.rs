use anyhow::Result;
use common::{NetworkStat, RostiData, RostiMsg, RostiStatus};
use itertools::Itertools;
use rostilib::{Msg, Rostcoder};
use std::{
    collections::HashMap,
    env,
    fs::{read_dir, read_to_string},
    sync::{
        atomic::{AtomicBool, Ordering},
        Arc, RwLock,
    },
    thread,
    time::Duration,
};
use systemstat::Platform;
use tokio::runtime::Runtime;

#[cfg(feature = "web-server")]
mod web;

#[cfg(feature = "obs-server")]
mod obs;
mod ws;

//#[tokio::main]
fn main() -> Result<()> {
    pretty_env_logger::init();
    let (rosti, rosti_rx, rosti_config) = Rostcoder::new_default_config()?;
    let rosti_config = Arc::new(RwLock::new(rosti_config));
    let (tx, _rx) = tokio::sync::broadcast::channel(16);

    let tx2 = tx.clone();

    let data = RostiData::default();
    let global_data = Arc::new(RwLock::new(data));
    let global_data2 = global_data.clone();

    let rosti2 = rosti.clone();

    #[cfg(feature = "obs-server")]
    {
        let tx_obs = tx.clone();
        let tx_obs = tx.clone();
        let rosti_obs = rosti.clone();
    }

    let restart = Arc::new(AtomicBool::new(false));
    let restart_ws = restart.clone();

    let rt = Runtime::new()?;
    let handle = rt.handle().clone();
    let rosti_config_ws = rosti_config.clone();
    rt.spawn(async move {
        #[cfg(feature = "web-server")]
        {
            let rosti2 = rosti.clone();
            handle.spawn(ws::start_ws_server(
                rosti2,
                tx,
                global_data,
                rosti_config_ws,
                restart_ws,
            ));
            handle.spawn(web::run(rosti));
        }
        #[cfg(not(feature = "web-server"))]
        start_ws_server(rosti, tx, global_data).await;
    });

    let mut time = std::time::Instant::now();
    let sys = systemstat::System::new();
    //let networks = sys.networks().unwrap();
    //let dev: Vec<&String> = networks
    //    .keys()
    //    .filter_map(|item| {
    //        if !item.contains("lo") && !item.contains("wg") && !item.contains("dummy") {
    //            Some(item)
    //        } else {
    //            None
    //        }
    //    })
    //    .collect();

    //TODO make loop smarter
    let mut data = RostiData::default();

    let paths = read_dir("/sys/class/thermal/")
        .or(read_dir("/sys/class/hwmon/"))
        .unwrap()
        .filter_map(|p| match p {
            Ok(p) => Some(p),
            Err(_) => None,
        })
        .map(|p| {
            let full_path = p.path().join("type");
            let path = p.path();

            let t = read_to_string(full_path)
                .unwrap_or("".to_string())
                .replace('\n', "")
                .replace('\t', "");
            (t, path)
        })
        .collect::<HashMap<String, std::path::PathBuf>>();

    let _network_map: HashMap<&&String, u64> = HashMap::new();

    #[cfg(feature = "obs-server")]
    thread::spawn(|| {
        if let Err(e) = obs::run(tx_obs, rosti_config, rosti_obs) {
            log::error!("Error while running obs provider: {:?}", e);
        }
    });

    loop {
        //getting msg from rosti
        if let Ok(msg) = rosti_rx.try_recv() {
            log::trace!("rosti msg: {:?}", msg);
            match msg {
                Msg::Start => data.status = RostiStatus::Online,
                Msg::Starting => data.status = RostiStatus::Connecting,
                Msg::Stop => {
                    data.status = RostiStatus::Offline;
                    data.curr_bitrate = 0;
                }
                Msg::Error(_) => {
                    data.status = RostiStatus::Connecting;
                    data.curr_bitrate = 0;
                    tx2.send(RostiMsg::Error(common::Error::LostConnection))?;
                    if restart.load(Ordering::Relaxed) {
                        rosti2.restart().unwrap();
                    }
                }
                Msg::BitrateUpdate(bitrate) => data.curr_bitrate = bitrate,
                Msg::Stalled => {
                    data.status = RostiStatus::Offline;
                    data.curr_bitrate = 0;
                    tx2.send(RostiMsg::Error(common::Error::LostVideoSrc))?;
                }
                Msg::Restart => {
                    tx2.send(RostiMsg::Info("Restarting...".to_string()))?;
                }
                Msg::AudioChannels(channels) => {
                    let mut left_right = [0; 2];
                    for (i, c) in channels.iter().enumerate() {
                        let c = c.min(&60);
                        match i {
                            0 | 1 => left_right[i] = *c,
                            _ => break,
                        }
                    }
                    tx2.send(RostiMsg::AudioChannel(left_right))?;
                }
                _ => {}
            }
        }

        if time.elapsed().as_secs() >= 1 {
            let old_network_stats = data.network_stats.clone();
            data.network_stats.clear();
            let dev_stats = procfs::net::dev_status();
            if let Ok(stats) = dev_stats {
                for name in stats.keys().sorted() {
                    if !name.contains("lo") && !name.contains("wg") && !name.contains("dummy") {
                        let dev_stat = &stats[name];
                        let k_byte = match old_network_stats.iter().find(|x| &x.name == name) {
                            Some(data) => (dev_stat.sent_bytes - data.tx_bytes) / 1000,
                            None => dev_stat.sent_bytes / 1000,
                        };
                        let n_stat = NetworkStat::new(&dev_stat.name, dev_stat.sent_bytes, k_byte);
                        data.network_stats.push(n_stat);
                    }
                }
            }
            //for d in &dev {
            //    if let Ok(stats) = sys.network_stats(d) {
            //        let mut k_byte = 0;
            //        match network_map.get_mut(d) {
            //            Some(res) => {
            //                let last = res.clone();
            //                let data = stats.tx_bytes.as_u64();
            //                k_byte = (data - last) / 1000;
            //                *res = data;
            //            }
            //            None => {
            //                network_map.insert(d, stats.tx_bytes.as_u64());
            //            }
            //        }
            //        let n = NetworkStat::new(
            //            d,
            //            &stats.tx_bytes.to_string(),
            //            k_byte, //&format!("{} KB/s", k_byte.to_string()),
            //        );
            //        data.network_stats.push(n);
            //    }
            //}
            match env::var("ZONE_TYPE") {
                Ok(v) => {
                    let temp_path = paths.get(&v);
                    if let Some(temp_path) = temp_path {
                        let temp = read_to_string(temp_path.join("temp"))
                            .unwrap_or("".to_string())
                            .replace('\n', "")
                            .replace('\r', "");
                        let temp: f32 = temp.parse().unwrap_or(0.0) / 1000.0;
                        data.temp = temp;
                    }
                }
                Err(_) => data.temp = sys.cpu_temp().unwrap_or(0.0),
            }
            time = std::time::Instant::now();
        }

        let d: RostiData = global_data2.read().unwrap().clone();
        if data != d {
            let mut d = global_data2.write().unwrap();
            *d = data.clone();
            let _ = tx2.send(RostiMsg::Data(data.clone()));
        }

        thread::sleep(Duration::from_millis(50));
    }
}
