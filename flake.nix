{
  nixConfig = {
    substituters = [
      "https://cache.nixos.org/"
      "https://nix-community.cachix.org"
      "https://nix.cache.vapor.systems"
    ];
    trusted-public-keys = [
      "cache.nixos.org-1:6NCHdD59X431o0gWypbMrAURkbJ16ZPMQFGspcDShjY="
      "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs="
      "nix.cache.vapor.systems-1:OjV+eZuOK+im1n8tuwHdT+9hkQVoJORdX96FvWcMABk="
    ];
  };

  inputs = {
    nixpkgs = {
      url = "github:nixos/nixpkgs/nixos-22.11";
    };

    fenix = {
      url = "github:nix-community/fenix";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    crane = {
      url = "github:ipetkov/crane";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, utils, fenix, crane }:
    with nixpkgs.lib;
    utils.lib.eachDefaultSystem (system:
      let
        #tpkgs = nixpkgs.legacyPackages.${system};
        #libtool = tpkgs.pkgsCross.aarch64-multiplatform-musl.pkgsStatic.callPackage ./nix/libtool.nix {  };
        #pkgs = import nixpkgs {
        #  inherit system;
        #  config = {
        #    allowUnfree = true;
        #    allowBroken = true;
        #    #packageOverrides = pkgs: {
        #    #  autoreconfHook = pkgs.autoreconfHook.override { libtool = libtool; };
        #    #};
        #  };
        #};
        pkgs = nixpkgs.legacyPackages.${system};
        pkgsArm = nixpkgs.legacyPackages.aarch64-linux;
        pkgsCross = pkgs.pkgsCross.aarch64-multiplatform-musl;

        rustToolchain = with fenix.packages.${system};
          combine [
            stable.toolchain
            targets.x86_64-unknown-linux-musl.stable.rust-std
            targets.wasm32-unknown-unknown.stable.rust-std
            targets.aarch64-unknown-linux-musl.stable.rust-std
            targets.aarch64-unknown-linux-gnu.stable.rust-std
          ];

        buildInputs = with pkgs; [
          pkgsi686Linux.glibc
          libclang
          autoPatchelfHook
        ];

        nativeBuildInputs = with pkgs; [
          #glibc_multi
          rustToolchain
          pkg-config
          cmake
          gst_all_1.gstreamer
          gst_all_1.gst-plugins-base
          gst_all_1.gst-plugins-good
          gst_all_1.gst-plugins-ugly
          gst_all_1.gst-plugins-bad
          openssl
          autoPatchelfHook
          llvmPackages.bintools

          lld
          trunk
          #wasm-bindgen-cli
          #wasm-bindgen-cli-0_2_79
        ];

        # crane setup
        craneLib = crane.lib.${system}.overrideToolchain rustToolchain;

        htmlFilter = path: _type: builtins.match ".*html$" path != null;
        htmlOrCargo = path: type:
          (htmlFilter path type) || (craneLib.filterCargoSources path type);
        src = nixpkgs.lib.cleanSourceWith {
          src = ./.; # The original, unfiltered source
          filter = htmlOrCargo;
        };

        cargoArtifactsServer = craneLib.buildDepsOnly {
          inherit src buildInputs nativeBuildInputs BINDGEN_EXTRA_CLANG_ARGS
            LIBCLANG_PATH OPENSSL_DIR OPENSSL_LIB_DIR OPENSSL_CRYPTO_LIBRARY
            OPENSSL_INCLUDE_DIR;

          cargoExtraArgs = "--package rostimote-server";
        };

        cargoArtifactsWasm = craneLib.buildDepsOnly {
          inherit src buildInputs nativeBuildInputs
            CARGO_TARGET_WASM32_UNKNOWN_UNKNOWN_LINKER;

          cargoExtraArgs = "--lib --package rostimote";
          CARGO_BUILD_TARGET = "wasm32-unknown-unknown";

          doCheck = false;
        };

        cargoArtifactsCross = craneLib.buildDepsOnly {
          inherit src nativeBuildInputs;
          inherit (aarch64-env)
            buildInputs BINDGEN_EXTRA_CLANG_ARGS LIBCLANG_PATH OPENSSL_STATIC
            OPENSSL_DIR OPENSSL_LIB_DIR TARGET_CC TARGET_CXX
            OPENSSL_CRYPTO_LIBRARY OPENSSL_INCLUDE_DIR CARGO_BUILD_TARGET
            CARGO_TARGET_AARCH64_UNKNOWN_LINUX_MUSL_LINKER;

          cargoExtraArgs = "--package rostimote-server";

          CROSS_BUILD = true;
        };

        ###############################################3
        # Environment Variables

        CARGO_TARGET_WASM32_UNKNOWN_UNKNOWN_LINKER = "wasm-ld";

        ROSTIMOTE_STATIC_PATH = "${self.packages."${system}".rostimote}/static";

        env = let
          openssl = pkgs.openssl_1_1;
        in {
          BINDGEN_EXTRA_CLANG_ARGS =
            "-I${pkgs.glibc.dev}/include -I${pkgs.libclang.lib}/lib/clang/11.1.0/include";

          LIBCLANG_PATH = "${pkgs.libclang.lib}/lib";

          OPENSSL_DIR = "${openssl}";
          OPENSSL_LIB_DIR = "${openssl.out}/lib";
          OPENSSL_CRYPTO_LIBRARY = "${openssl.out}/lib";
          OPENSSL_INCLUDE_DIR = "${openssl.dev}/include";
        };

        aarch64-env = let
          openssl = pkgsCross.pkgsStatic.openssl_1_1;
          #elfutils = pkgsCross.pkgsStatic.callPackage ./nix/elf.nix {  };
          #gstreamer = pkgsCross.pkgsStatic.callPackage ./nix/default.nix {
          #};
          gstreamer = pkgsCross.gst_all_1.gstreamer;
          #gstreamer = pkgsCross.pkgsStatic.gst_all_1.gstreamer;
          target = "aarch64-unknown-linux-musl";
        in {
          CARGO_BUILD_TARGET = target;

          LIBCLANG_PATH = "${pkgs.libclang.lib}/lib";

          TARGET_CC = "${pkgsCross.stdenv.cc}/bin/${target}-gcc";
          TARGET_CXX = "${pkgsCross.stdenv.cc}/bin/${target}-g++";

          CARGO_TARGET_WASM32_UNKNOWN_UNKNOWN_LINKER = "wasm-ld";
          CARGO_TARGET_AARCH64_UNKNOWN_LINUX_MUSL_LINKER =
            "${pkgsCross.stdenv.cc}/bin/${target}-gcc";

          BINDGEN_EXTRA_CLANG_ARGS =
            "-I${pkgsArm.glibc.dev}/include -I${pkgsArm.libclang.lib}/lib/clang/11.1.0/include";

          OPENSSL_STATIC = 1;
          OPENSSL_DIR = "${openssl}";
          OPENSSL_LIB_DIR = "${openssl.out}/lib";
          OPENSSL_CRYPTO_LIBRARY = "${openssl.out}/lib";
          OPENSSL_INCLUDE_DIR = "${openssl.dev}/include";


          buildInputs = buildInputs ++ [
            openssl
            gstreamer
            pkgsCross.pkgsStatic.libclang
            #pkgsCross.pkgsStatic.gst_all_1.gstreamer
            pkgsCross.gst_all_1.gst-plugins-base
          ];
        };
      in {
        packages = {
          rostimote = craneLib.buildPackage {
            inherit buildInputs nativeBuildInputs
              CARGO_TARGET_WASM32_UNKNOWN_UNKNOWN_LINKER;

            pname = "rostimote";

            src = src;
            cargoArtifacts = cargoArtifactsWasm;
            cargoExtraArgs = "--lib --package rostimote";

            doCheck = false;

            CARGO_BUILD_TARGET = "wasm32-unknown-unknown";

            installPhase = ''
              mkdir -p $out/static
              wasm-bindgen target/wasm32-unknown-unknown/release/rostimote.wasm \
                --out-dir $out/static/ --no-modules --no-typescript
            '';
          };

          rostimote-server = craneLib.buildPackage {
            inherit buildInputs nativeBuildInputs ROSTIMOTE_STATIC_PATH;
            inherit (env)
              BINDGEN_EXTRA_CLANG_ARGS LIBCLANG_PATH OPENSSL_DIR OPENSSL_LIB_DIR
              OPENSSL_CRYPTO_LIBRARY OPENSSL_INCLUDE_DIR;

            pname = "rostimote-server";

            src = src;
            cargoArtifacts = cargoArtifactsServer;
            cargoExtraArgs = "--package rostimote-server";

            CROSS_BUILD = true;
          };

          rostimote-server-cross = craneLib.buildPackage {
            inherit src nativeBuildInputs ROSTIMOTE_STATIC_PATH;
            inherit (aarch64-env)
              buildInputs BINDGEN_EXTRA_CLANG_ARGS LIBCLANG_PATH OPENSSL_STATIC
              OPENSSL_DIR OPENSSL_LIB_DIR OPENSSL_CRYPTO_LIBRARY
              OPENSSL_INCLUDE_DIR CARGO_BUILD_TARGET TARGET_CC TARGET_CXX
              CARGO_TARGET_AARCH64_UNKNOWN_LINUX_MUSL_LINKER;

            pname = "rostimote-server";
            cargoArtifacts = cargoArtifactsCross;

            cargoExtraArgs = "--package rostimote-server";

            CROSS_BUILD = true;
          };
        };

        devShells = {
          default = pkgs.mkShell {
            inherit buildInputs nativeBuildInputs
              CARGO_TARGET_WASM32_UNKNOWN_UNKNOWN_LINKER;
            inherit (env)
              BINDGEN_EXTRA_CLANG_ARGS LIBCLANG_PATH OPENSSL_DIR OPENSSL_LIB_DIR
              OPENSSL_CRYPTO_LIBRARY OPENSSL_INCLUDE_DIR;
          };

          cross = pkgsCross.mkShell {
            inherit nativeBuildInputs;
            inherit (aarch64-env)
              buildInputs BINDGEN_EXTRA_CLANG_ARGS LIBCLANG_PATH OPENSSL_STATIC
              OPENSSL_DIR OPENSSL_LIB_DIR OPENSSL_CRYPTO_LIBRARY
              OPENSSL_INCLUDE_DIR CARGO_BUILD_TARGET
              CARGO_TARGET_AARCH64_UNKNOWN_LINUX_MUSL_LINKER;
          };
        };
      });
}
